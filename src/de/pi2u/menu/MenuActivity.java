package de.pi2u.menu;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import de.pi2u.gl.MainActivity;
import de.pi2u.menu.R;
import de.pi2u.menu.MenuActivity;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

/**
 * 
 * @author Patrick
 */
public class MenuActivity extends Activity {

	public Settings	setting;
	Spinner			spinner;


	/**
	 * 
	 * @param savedInstanceState
	 */
	@Override
	protected void onCreate( Bundle savedInstanceState ) {

		super.onCreate(savedInstanceState);

		setting = ((Settings) getApplicationContext());

		setContentView(R.layout.menu);
		spinner = (Spinner) findViewById(R.id.spinner1);

		List<String> list = new ArrayList<String>();
		list.add("Internal");

		File jo = new File(setting.mapfolder);

		if (jo.listFiles() != null) {
			for (String e : jo.list()) {
				System.out.println("File: " + e);
				if (e.endsWith(".bmp")) {
					list.add(e);
				}
			}
		}

		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);

	}


	public void start_game( View v ) {

		Intent intent = new Intent(this, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);

		setting.map = String.valueOf(spinner.getSelectedItem());

		System.out.println("Map: " + setting.map);
		startActivity(intent);
		setting.running = false;

	}


	public void exit( View v ) {

		finish();

		android.os.Process.killProcess(android.os.Process.myPid());
	}


	@Override
	protected void onPause() {
		super.onPause();

	}


	@Override
	protected void onResume() {
		super.onResume();

	}


	@Override
	protected void onStop() {
		super.onStop();

		// renderer.onStop();
	}


	@Override
	public void onConfigurationChanged( Configuration newConfig ) {

		super.onConfigurationChanged(newConfig);
	}


	@Override
	public boolean onTouchEvent( MotionEvent me ) {

		try {
			Thread.sleep(10);
		}
		catch (Exception e) {
		}

		return super.onTouchEvent(me);
	}


	/**
	 * 
	 * @return
	 */
	protected boolean isFullscreenOpaque() {
		return true;
	}
}
