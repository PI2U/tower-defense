/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pi2u.gl;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * 
 * @author Patrick
 */
public class RingList<E> extends ArrayList<E> {

	public int iterator = 0;
	
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 123L;



	public RingList() {
		super();
		
	}



	public   E getNext() {
		
		if(iterator == size()){
			iterator = 0;
			return get(0);
		}

		return get(iterator++);
	}

}
