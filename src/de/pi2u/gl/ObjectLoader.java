/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pi2u.gl;

import java.util.HashMap;

import com.threed.jpct.Object3D;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;

/**
 * 
 * @author Patrick
 */
public class ObjectLoader {

	public static HashMap<String, PooledObject> pooledObjects = new HashMap<String, PooledObject>();
	
	public ObjectLoader() {

	}

	public static Object3D[] loadTower3DS(int model, float size, boolean ser,int... textur) {

		int length = 1;
		// tower = Loader.load3DS(con.getResources().openRawResource(model),
		// scale);
		if (ser == false) {
			length = com.threed.jpct.Loader.load3DS(MainActivity.renderer.con.getResources().openRawResource(model), size).length;
		}

		Texture tow;

		// sharecompileddata(obj) geht komischerweise nicht, würde aber eine
		// menge GPU Speicher sparen

		Object3D[] tower = new Object3D[length];

		for (int i = 0; i < length; i++) {

			if (ser) {
				tower[i] = com.threed.jpct.Loader.loadSerializedObject(MainActivity.renderer.con.getResources().openRawResource(model));
				tower[i].scale(size);
				tower[i].translate(0, 0, -3f);

			} else {
				tower[i] = com.threed.jpct.Loader.load3DS(MainActivity.renderer.con.getResources().openRawResource(model), size)[i];

			}

			if (!(TextureManager.getInstance().containsTexture(String.valueOf(textur[i])))) {

				tow = new Texture(MainActivity.renderer.con.getResources().openRawResource(textur[i]));
				tow.compress();
				TextureManager.getInstance().addTexture(String.valueOf(textur[i]), tow);
			}

			tower[i].setTexture(String.valueOf(textur[i]));
			tower[i].strip();

		}

		return tower;
		
	}

	
	
	public static Object3D[] loadTowerOBJ( int model, int mtl, float size, boolean ser, int... textur) {

		int length = 1;

		if (ser == false) {
			length = com.threed.jpct.Loader.loadOBJ(MainActivity.renderer.con.getResources().openRawResource(model),MainActivity.renderer.con.getResources().openRawResource(mtl), size).length;
		}

		Texture tow;

		// sharecompileddata(obj) geht komischerweise nicht, würde aber eine
		// menge GPU Speicher sparen

		Object3D[] tower = new Object3D[length];

		for (int i = 0; i < length; i++) {

			if (ser) {
				tower[i] = com.threed.jpct.Loader.loadSerializedObject(MainActivity.renderer.con.getResources().openRawResource(model));
				tower[i].scale(0.3f);
				tower[i].translate(0, 0, -3f);

			} else {
				tower[i] = com.threed.jpct.Loader.loadOBJ(MainActivity.renderer.con.getResources().openRawResource(model),MainActivity.renderer.con.getResources().openRawResource(mtl), size)[i];
				tower[i].translate(0, 0, 0.6f);
			}

			if (!(TextureManager.getInstance().containsTexture(String.valueOf(textur[i])))) {

				tow = new Texture(MainActivity.renderer.con.getResources().openRawResource(textur[i]));
				tow.compress();
				TextureManager.getInstance().addTexture(String.valueOf(textur[i]), tow);
			}

			tower[i].setTexture(String.valueOf(textur[i]));
			tower[i].strip();

		}

		return tower;

	}


}
