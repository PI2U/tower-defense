package de.pi2u.gl;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

public class GLView extends GLSurfaceView {

	public GLView(Context context, AttributeSet attrs) {
		super(context, attrs);

		setEGLContextClientVersion(1);

		setEGLConfigChooser(new GLSurfaceView.EGLConfigChooser() {
			public EGLConfig chooseConfig( EGL10 egl , EGLDisplay display ) {
				// Ensure that we get a 16bit framebuffer. Otherwise, we'll fall
				// back to Pixelflinger on some device (read: Samsung I7500)

				int[] attributes = new int[] { EGL10.EGL_DEPTH_SIZE, 4, EGL10.EGL_GREEN_SIZE, 2, EGL10.EGL_BLUE_SIZE, 2, EGL10.EGL_RED_SIZE, 2, EGL10.EGL_NONE };
				EGLConfig[] configs = new EGLConfig[1];
				int[] result = new int[1];

				egl.eglChooseConfig(display, attributes, configs, 1, result);

				return configs[0];
			}
		});

		this.getHolder().setFormat(PixelFormat.TRANSPARENT);

	}


}
