/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pi2u.gl;

import com.threed.jpct.Matrix;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;

/**
 * 
 * @author Patrick
 */
public class Monster {

	public int				hp			= 1000, zielX, zielY, radius = 2, i = 0, texId, hpOrig;
	public Object3D			monster;
	public Object3D			hpBar		= new Object3D(2);
	public float			actPosX, actPosY, speed = 0.03f;
	public Pathfinder		pathfinder;
	public boolean			da			= true;
	public SimpleVector		spawn		= new SimpleVector();
	private Point[]			path,oldpath;
	Matrix					mat			= new Matrix();
	float					tb			= 0;
	public boolean			changedPath	= false;


	public Monster() {

	}


	public Monster(Spawnpoint spawnpoint, Object3D obj) {
		add(spawnpoint, obj);
	}


	public Monster add( Spawnpoint spawnpoint , Object3D obj ) {
		hpOrig = hp;
		monster = new Object3D(obj.cloneObject());
		obj = null;
		spawn.x = spawnpoint.spawn.x;
		spawn.y = spawnpoint.spawn.y;
		spawn.z = -0.6f;

		monster.translate(spawn);
		monster.scale(0.5f);
		monster.build();

		actPosX = spawn.x;
		actPosY = spawn.y;

		path = spawnpoint.path;
		pathfinder = new Pathfinder(spawnpoint.spawn, MainActivity.renderer.map.end);

		float hoehe = -0.05f;
		float breite = -0.7f;
		float tb = 0.5f;

		hpBar.addTriangle(SimpleVector.create(0, 0, 0f), 0, 0f, SimpleVector.create(breite, 0, 0f), tb, 0f, SimpleVector.create(0, hoehe, 0f), 0f, tb);
		hpBar.addTriangle(SimpleVector.create(breite, hoehe, 0f), tb, 0, SimpleVector.create(0, hoehe, 0f), 0f, tb, SimpleVector.create(breite, 0f, 0f), tb, 0f);
		hpBar.setTexture("hpBar");

		MainActivity.renderer.world.addObject(hpBar);
		MainActivity.renderer.world.addObject(monster);

		hpBar.setTextureMatrix(mat);

		return this;
	}


	public void hit( int dmg ) {
		hp -= dmg;
		mat.set(3, 0, -tb);
		hpBar.setTextureMatrix(mat);
		tb = (hpOrig - hp) * 0.005f;
		if (hp <= 0) {
			kill();
		}
	}


	public void update() {

		hpBar.setTranslationMatrix(monster.getTranslationMatrix().cloneMatrix());
		hpBar.translate(0.45f, 0, 0.8f);

		if( changedPath ){
			oldpath = path.clone();
			try {
				pathfinder.reset();
				pathfinder.start = path[i];
				path = pathfinder.calc();
				i=0;
				changedPath = false;
			}
			catch (Exception e) {
				System.err.println("Error in recalcPath()");
				path = oldpath.clone();
				changedPath = false;
			}
		}
		if (da == false) {

			if (actPosX < zielX) {
				monster.translate(speed, 0, 0);
				actPosX += speed;
			}
			else if (actPosX > zielX + speed) {
				monster.translate(-speed, 0, 0);
				actPosX -= speed;
			}

			if (actPosY < zielY) {
				monster.translate(0, speed, 0);
				actPosY += speed;
			}
			else if (actPosY > zielY + speed) {
				monster.translate(0, -speed, 0);
				actPosY -= speed;
			}

			if ((int) actPosX >= zielX - 1 & (int) actPosX <= zielX & (int) actPosY >= zielY - 1 & (int) actPosY <= zielY) {
				da = true;
				zielX = (int) actPosX;
				zielY = (int) actPosY;
			}

		}
		else {
			if(path == null){
				return;
			}
			goTo(path[i].x, path[i].y);
			Util.lookAt(monster, path[i].x, path[i].y);
			i++;
			if (i == path.length) {

				kill();

				// i = 0;
				// monster.clearTranslation();
				// spawn.x = path[0].x;
				// spawn.y = path[0].y;
				// monster.translate(spawn);
				// actPosX = path[0].x;
				// actPosY = path[0].y;
				// zielX = path[0].x;
				//
				// zielY = path[0].y;
				// da = false;
			}
		}
	}


	public void goTo( int x , int y ) {
		zielX = x;
		zielY = y;
		da = false;

	}


	public void kill() {
		MainActivity.renderer.monster.remove(this);
		MainActivity.renderer.world.remove(monster);
		MainActivity.renderer.world.remove(hpBar);

		monster = null;
		hpBar = null;
	}
}
