/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pi2u.gl;

import java.util.Random;

import com.threed.jpct.*;

/**
 * 
 * @author Patrick
 */
public class Tower {

	public int			dmg		= 1;
	public float		radius	= 3f;
	public Texture		texture;
	public boolean		splash, poison, slow, multi, pierce, ice, remove;
	public int			posX, posY;
	public Object3D[]	tower;
	double				alpha	= 0.0;
	SimpleVector		xx, yy, center;
	Point				pos;
	SimpleVector		tmp		= new SimpleVector(0, 0, 0);
	Monster				focus;
	Object3D			bullet;
	int					update	= 0;
	long				timeOld, timeBullet;
	Random				rnd;


	// Höhe noch hardcoded, model ist die 3ds Datei
	public Tower(int x, int y, float scale, Object3D[] obj, Object3D bullet) throws Exception {

		System.out.println("Add tower x: " + x + " y: " + y);

		if (MainActivity.renderer.loading == false) {

			if (MapLoader.grid[x][y] == true || (x == MainActivity.renderer.map.end.x && y == MainActivity.renderer.map.end.y)) {
				throw new Exception("Schon etwas vorhanden");
			}
			else if (x > MapLoader.bitmapWidth || y > MapLoader.bitmapHeight) {
				throw new Exception("ausserhalb der Map");
			}

			MapLoader.grid[x][y] = true;

			Point tmp = new Point(x, y);

			for (Spawnpoint e : MainActivity.renderer.map.spawnpunkt) {
				try {

					if (e.checkInPath(tmp)) {
						System.err.println("Point: " + tmp.toString() + " in path");
						e.update();
						for (Monster e1 : MainActivity.renderer.monster) {
							e1.changedPath = true;
						}
					}
				}
				catch (Exception e1) {
					MapLoader.grid[x][y] = false;
					throw new Exception("Kein Weg danach");
				}
			}

		}

		tower = new Object3D[obj.length];
		this.bullet = bullet.cloneObject();
		this.bullet.scale(3);
		rnd = new Random();

		for (int i = 0; i < obj.length; i++) {

			tower[i] = obj[i];
			if (i == 0) {
				tower[i].addChild(this.bullet);

			}
			tower[i].translate(x, y, 0f);
			tower[i].scale(scale);
			tower[i].strip();

			MainActivity.renderer.world.addObject(tower[i]);

		}
		MainActivity.renderer.world.addObject(this.bullet);
		center = tower[0].getTransformedCenter();

		pos = new Point(x, y);

		MainActivity.renderer.tower.add(this);

	}


	public Tower(int round, int round2, float scale, PooledObject pooledObject, Object3D bullet2) throws Exception {

		this(round, round2, scale, pooledObject.obj, bullet2);
	}


	public void setPosition( int id , int x , int y ) {

		for (int i = 0; i < tower.length; i++) {
			tower[i].translate(x, y, 0);
		}

	}


	public void lookAt( Monster focus ) {
		// Geogebra Funktion zur Berechnung des Winkels zwischen 2 Vektoren
		// Winkel[arcos((ax*bx+ay*by)/(sqrt(ax^2+ay^2)*sqrt(bx^2+by^2)))]

		yy = tower[0].getXAxis().normalize();
		xx = focus.monster.getTransformedCenter().calcSub(center).normalize();
		tmp.x = yy.y * -1;
		tmp.y = yy.x;

		alpha = Math.atan2(xx.x, xx.y) - Math.atan2(tmp.x, tmp.y);

		tower[0].rotateZ((float) alpha);

	}


	public void fire( Monster focus ) {

		if (System.currentTimeMillis() - timeOld >= 10) {

			bullet.translate(0, center.distance(focus.monster.getTransformedCenter()) * 0.1f, -bullet.getTransformedCenter().y * 0.0005f);
			timeOld = System.currentTimeMillis();

		}

		if (bullet.getTransformedCenter().distance(center) > center.distance(focus.monster.getTransformedCenter())) {
			focus.hit(rnd.nextInt(dmg));
			bullet.clearTranslation();
			bullet.translate(new SimpleVector(0, 0, 0.5f));
		}

	}


	public void remove() {

		for (Object3D e : tower) {
			MainActivity.renderer.world.remove(e);
		}

		MainActivity.renderer.world.removeObject(bullet);
		MapLoader.grid[pos.x][pos.y] = false;
		MainActivity.renderer.tower.remove(this);
	}


	public void update() {

		if (remove) {
			remove();
			return;
		}

		float tmp = 0, distance = 100;

		for (Monster e : MainActivity.renderer.monster) {

			if (e != null) {
				tmp = center.distance(e.monster.getTransformedCenter());

				if (tmp <= distance) {
					distance = tmp;

					if (distance <= radius && this.focus == null) {
						this.focus = e;

					}
					if (distance > radius && this.focus != null) {
						this.focus = null;

					}

				}
			}
		}

		if (this.focus != null && this.focus.monster != null) {
			lookAt(this.focus);
			bullet.setVisibility(true);
			fire(this.focus);

		}
		else {
			focus = null;
			bullet.clearTranslation();
			bullet.setVisibility(false);
		}

	}

}
