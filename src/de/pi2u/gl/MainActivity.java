package de.pi2u.gl;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;

import de.pi2u.menu.R;
import de.pi2u.menu.Settings;
import android.app.Activity;
import android.content.res.Configuration;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ViewFlipper;

/**
 * 
 * @author Patrick
 */
public class MainActivity extends Activity {

	private GLView				mGLView;
	protected static Renderer	renderer	= null;
	private float				touchX, touchY, fingerdiff, camHeight;
	private int					deadZone	= 50;
	private float				camSpeed	= 0.20f / 7;
	ViewFlipper					flipper;
	SimpleVector				coords		= new SimpleVector();
	Object3D[]					tower;
	public Settings setting;

	/**
	 * 
	 * @param savedInstanceState
	 */
	@Override
	protected void onCreate( Bundle savedInstanceState ) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.ingame);
		setting = ((Settings)getApplicationContext());
		
		flipper = (ViewFlipper) findViewById(R.id.viewFlipper1);

		mGLView = (GLView) findViewById(R.id.surfaceView1);

		mGLView.setEGLContextClientVersion(2);

		WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
		layoutParams.flags = WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;

		getWindow().setAttributes(layoutParams);

		mGLView.setEGLConfigChooser(new GLSurfaceView.EGLConfigChooser() {
			public EGLConfig chooseConfig( EGL10 egl , EGLDisplay display ) {
				// Ensure that we get a 16bit framebuffer. Otherwise, we'll fall
				// back to Pixelflinger on some device (read: Samsung I7500)

				int[] attributes = new int[] { EGL10.EGL_DEPTH_SIZE, 4, EGL10.EGL_GREEN_SIZE, 4, EGL10.EGL_BLUE_SIZE, 4, EGL10.EGL_RED_SIZE, 4, EGL10.EGL_NONE };
				EGLConfig[] configs = new EGLConfig[1];
				int[] result = new int[1];

				egl.eglChooseConfig(display, attributes, configs, 1, result);

				return configs[0];
			}
		});

		//mGLView.setEGLConfigChooser(new AAConfigChooser(mGLView));
		

		renderer = new Renderer(this);

		

		
		
		mGLView.setRenderer(renderer);

	}


	protected void start_game( Bundle savedInstanceState ) {

		setContentView(R.layout.ingame);

		flipper = (ViewFlipper) findViewById(R.id.viewFlipper1);

		mGLView = (GLView) findViewById(R.id.surfaceView1);

		mGLView.setEGLContextClientVersion(2);

		WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
		layoutParams.flags = WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;

		getWindow().setAttributes(layoutParams);

		mGLView.setEGLConfigChooser(new GLSurfaceView.EGLConfigChooser() {
			public EGLConfig chooseConfig( EGL10 egl , EGLDisplay display ) {
				// Ensure that we get a 16bit framebuffer. Otherwise, we'll fall
				// back to Pixelflinger on some device (read: Samsung I7500)

				int[] attributes = new int[] { EGL10.EGL_DEPTH_SIZE, 4, EGL10.EGL_GREEN_SIZE, 4, EGL10.EGL_BLUE_SIZE, 4, EGL10.EGL_RED_SIZE, 8, EGL10.EGL_NONE };
				EGLConfig[] configs = new EGLConfig[1];
				int[] result = new int[1];

				egl.eglChooseConfig(display, attributes, configs, 1, result);

				return configs[0];
			}
		});

		//mGLView.setEGLConfigChooser(new AAConfigChooser(mGLView));
		renderer = new Renderer(this);

		mGLView.setRenderer(renderer);

	}


	// UI
	public void exit( View v ) {
		
		onStop();
	}


	public void togglePause( View v ) {
		renderer.pause ^= true;
	}


	public void toggleDebug( View v ) {
		renderer.debug ^= true;
	}


	public void towerChooser( View v ) {
		renderer.towerChooser = true;
		renderer.chooser.setVisibility(renderer.towerChooser);
		flipper.setDisplayedChild(2);
	}


	public void toggleMenu( View v ) {
		flipper.setDisplayedChild(1);

	}


	public void addTower( View v ) {

		
		try {
			new Tower(Math.round(coords.x), Math.round(coords.y), 0.8f, ObjectLoader.loadTower3DS( R.raw.tower2, 0.5f, false, R.raw.auv2, R.raw.auv1), renderer.map.bullet);

		}
		catch (Exception e) {
			System.err.println("Kann Tower nicht hinzufügen: ");
			System.err.println(e.getMessage());
			
		}

	}


	public void backtoMenu( View v ) {
		flipper.setDisplayedChild(0);
		renderer.towerChooser = false;
		renderer.chooser.setVisibility(renderer.towerChooser);
	}


	public void clear( View v ) {

		renderer.lock = true;
		for (Tower e : renderer.tower) {
			e.remove = true;;
		}
		for (Monster e : renderer.monster) {
			e.changedPath = true;
		}

		renderer.lock = false;
	}


	// Ende UI

	@Override
	protected void onPause() {
		super.onPause();

		if (mGLView != null) {
			mGLView.onPause();
		}
	}


	@Override
	protected void onResume() {
		super.onResume();
		// mGLView.onResume();

	}


	@Override
	protected void onStop() {
		super.onStop();

		setting.running = false;
		finish();

		// android.os.Process.killProcess(android.os.Process.myPid());

	}


	@Override
	public void onConfigurationChanged( Configuration newConfig ) {

		super.onConfigurationChanged(newConfig);
	}


	@Override
	public boolean onTouchEvent( MotionEvent me ) {

		if (me.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN) {
			fingerdiff = (float) Math.hypot(me.getX(0) - me.getX(1), me.getY(1) - me.getY(0));
		}

		// Leider noch nicht perfekt
		if (me.getAction() == MotionEvent.ACTION_DOWN) {
			touchX = me.getX();
			touchY = me.getY();

			if (renderer.towerChooser) {
				renderer.chooser.clearTranslation();

				coords = renderer.convertScreenPixelsTo3D((int) touchX, (int) touchY);

				renderer.chooser.translate(Math.round(coords.x), Math.round(coords.y), 0);

			}

			// if(Renderer.towerChooser = true){
			//
			// chooser.clearTranslation();
			// chooserPos =
			// convertScreenPixelsTo3D(fb.getWidth()/2,fb.getHeight()/2);
			// chooserPos.x = (int) chooserPos.x;
			// chooserPos.y = (int) chooserPos.y;
			// chooserPos.z = 0;
			//
			// chooser.translate(chooserPos);
			// chooser.setVisibility(true);
			//
			// }
		}

		if (me.getAction() == MotionEvent.ACTION_MOVE && me.getPointerCount() == 1) {
			camHeight = renderer.cam.getPosition().z;

			if (me.getX() < touchX - deadZone) {
				renderer.setCameraDiff(camSpeed * camHeight, 0, 0);

			}
			else if (me.getX() > touchX + deadZone) {
				renderer.setCameraDiff(-camSpeed * camHeight, 0, 0);

			}

			if (me.getY() < touchY - deadZone) {
				renderer.setCameraDiff(0, -camSpeed * camHeight, 0);
			}
			else if (me.getY() > touchY + deadZone) {
				renderer.setCameraDiff(0, camSpeed * camHeight, 0);
			}

		}
		else if (me.getAction() == MotionEvent.ACTION_MOVE && me.getPointerCount() > 1) {

			float temp = (float) Math.hypot(me.getX(0) - me.getX(1), me.getY(1) - me.getY(0));

			if (temp != fingerdiff) {
				renderer.setCameraDiff(0, 0, (fingerdiff - temp) / 50);
			}
			fingerdiff = (float) Math.hypot(me.getX(0) - me.getX(1), me.getY(1) - me.getY(0));
		}

		try {
			Thread.sleep(10);
		}
		catch (Exception e) {
		}

		return super.onTouchEvent(me);
	}


	/**
	 * 
	 * @return
	 */
	protected boolean isFullscreenOpaque() {
		return true;
	}
}
