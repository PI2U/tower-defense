/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pi2u.gl;

import java.util.*;

/**
 * 
 * @author Patrick
 */
public class Pathfinder {

	BinaryHeap geschlosseneListe = new BinaryHeap(60);
	BinaryHeap offeneListe = new BinaryHeap(60);
	LinkedList<Point> path = new LinkedList<Point>();
	Point start, ziel;
	

	public Pathfinder(Point start, Point ziel) {
		this.start = new Point(start.x, start.y, 0, start.calcH(ziel));
		this.ziel = ziel;
		

	}

	public void reset(){
		path.clear();
		geschlosseneListe.makeEmpty();
		offeneListe.makeEmpty();
	}
	
	public Point[] calc() throws Exception {

		Point act = start.copyTo();
		Point tmp2, tmp3;
		act.zeigeAuf(act);
		act.g = 0;
		Point tmp;

		while (act.h != 0) {

			for (int i = -1; i <= 1; i++) {
				for (int ii = -1; ii <= 1; ii++) {

					if (i != 0 || ii != 0) {
						if (geschlosseneListe.contains(act) == null) {

							int first = act.x + i;
							int second = act.y + ii;

							if (first >= 0 && first <= MapLoader.bitmapWidth - 1 && second >= 0 && second <= MapLoader.bitmapHeight - 1) {

								tmp = new Point(first, second, act);
								tmp.calcF(ziel);

								if (MapLoader.grid[first][second] == false) {

//									if (tmp.g > tmp.calcH(start)) {
//										tmp.g = tmp.calcH(start);
//									}

									tmp2 = offeneListe.contains(tmp);
									tmp3 = geschlosseneListe.contains(tmp);
									if ((tmp2 == null && tmp3 == null)) {
										if (tmp.diagonal(act)) {

										} else {
											offeneListe.insert(tmp);
										}
									}
								}
							}
						}
					}
				}
			}

			geschlosseneListe.insert(act);
			// System.out.println("(" + act.x + "," + act.y + ")" + " g: " +
			// act.g + "  h: " + act.h + " f: " + (act.g+act.h) );
			try{
			act = offeneListe.deleteMin();
			}catch(Exception e){
			
				throw e;
			}

		}

		while (act.g != 0) {
			// System.out.println("(" + act.x + "," + act.y + ")" + "->" );
			path.addFirst(act);
			int newX = act.zeigtAuf.x;
			int newY = act.zeigtAuf.y;

			if (newX == act.x + 1 && newY == act.y + 1) {

				if (MapLoader.grid[act.x + 1][act.y] == true) {
					act.zeigtAuf = new Point(act.x, act.y + 1, 1, 1, act.zeigtAuf);
				} else if (MapLoader.grid[act.x][act.y + 1] == true) {
					act.zeigtAuf = new Point(act.x + 1, act.y, 1, 1, act.zeigtAuf);
				}

			} else if (newX == act.x - 1 && newY == act.y + 1) {

				if (MapLoader.grid[act.x - 1][act.y] == true) {
					act.zeigtAuf = new Point(act.x, act.y + 1, 1, 1, act.zeigtAuf);
				} else if (MapLoader.grid[act.x][act.y + 1] == true) {
					act.zeigtAuf = new Point(act.x - 1, act.y, 1, 1, act.zeigtAuf);
				}

			} else if (newX == act.x + 1 && newY == act.y - 1) {

				if (MapLoader.grid[act.x + 1][act.y] == true) {
					act.zeigtAuf = new Point(act.x, act.y - 1, 1, 1, act.zeigtAuf);
				} else if (MapLoader.grid[act.x][act.y - 1] == true) {
					act.zeigtAuf = new Point(act.x + 1, act.y, 1, 1, act.zeigtAuf);
				}

			} else if (newX == act.x - 1 && newY == act.y - 1) {

				if (MapLoader.grid[act.x - 1][act.y] == true) {
					act.zeigtAuf = new Point(act.x, act.y - 1, 1, 1, act.zeigtAuf);
				} else if (MapLoader.grid[act.x][act.y - 1] == true) {
					act.zeigtAuf = new Point(act.x - 1, act.y, 1, 1, act.zeigtAuf);
				}
			}

			act = act.zeigtAuf;
		}
		
		path.addFirst(act);

		Point[] jo = new Point[1];

		return path.toArray(jo);
	}
	
	public Point[] getPath(){
		Point[] jo = new Point[1];

		return path.toArray(jo);
	}
	
}
