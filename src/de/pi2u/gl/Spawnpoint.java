package de.pi2u.gl;

import java.util.ArrayList;

import com.threed.jpct.Object3D;
import com.threed.jpct.RGBColor;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;

public class Spawnpoint {

	public Point		spawn, end;
	public Point[]		path;
	public Pathfinder	pathfinder;


	public Spawnpoint(Point point) {
		add(point);

	}


	public void add( Point point ) {
		spawn = point;
	}


	public void setEnd( Point end ) {
		this.end = end;
		pathfinder = new Pathfinder(spawn, end);
		try {
			update();
		}
		catch (Exception e) {
			System.err.println("dtjtfhd");
			e.printStackTrace();
		}

	}


	public void print() {
		System.out.println("Spawn :" + spawn.toString() + " Ende: " + end.toString());

	}


	public boolean checkInPath( Point test ) {
		for (Point e : path) {
			if (e.equals(test)) {
				return true;
			}
		}
		return false;
	}

	//ArrayList<Object3D>	debug	= new ArrayList<Object3D>();


	public void update() throws Exception {

		
		try {
			pathfinder.reset();
			path = pathfinder.calc();
		}
		catch (Exception e) {
			System.err.println("awfwddw");
			throw new Exception();
		}
		

		
//		try {
//			for (Object3D e : debug) {
//				e.clearObject();
//				e = null;
//			}
//		}
//		catch (Exception e) {
//			System.err.println("Bdadw");
//		}

		//debug	= new ArrayList<Object3D>();



//		Object3D DebugMarker = new Object3D(2);
//		debug.add(DebugMarker);
//		Texture tex = new Texture(1, 1, new RGBColor(spawn.x * 4, spawn.y * 4, end.x * 4));
//		TextureManager.getInstance().addTexture("Bla" + tex.hashCode(), tex);
//
//		for (Point e : path) {
//			DebugMarker = new Object3D(2);
//			debug.add(DebugMarker);
//			DebugMarker.addTriangle(SimpleVector.create(0, 0, 0f), 0, 0f, SimpleVector.create(1, 0, 0f), 1, 0f, SimpleVector.create(0, 1, 0f), 0f, 1);
//			DebugMarker.addTriangle(SimpleVector.create(1, 1, 0f), 1, 0, SimpleVector.create(0, 1, 0f), 0f, 1, SimpleVector.create(1, 0f, 0f), 1, 0f);
//			DebugMarker.translate(e.x - 0.5f, e.y - 0.5f, 0.1f);
//			DebugMarker.setTexture("Bla" + tex.hashCode());
//			MainActivity.renderer.world.addObject(DebugMarker);
//		}

	}


}
