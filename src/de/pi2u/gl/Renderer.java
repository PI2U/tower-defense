/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pi2u.gl;

import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.annotation.SuppressLint;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Environment;
import de.pi2u.menu.R;
import de.pi2u.menu.Settings;

import com.threed.jpct.Camera;
import com.threed.jpct.Config;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Interact2D;
import com.threed.jpct.Light;
import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;
import com.threed.jpct.RGBColor;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.util.BitmapHelper;
import com.threed.jpct.util.MemoryHelper;

/**
 * 
 * @author Patrick
 */
@SuppressLint({ "Registered", "DefaultLocale" })
class Renderer extends MainActivity implements GLSurfaceView.Renderer {

	public FrameBuffer		fb		= null;
	public ThreadSafeWorld	world	= null;
	public RGBColor			back	= new RGBColor(0, 0, 0);
	public Light			sun;
	public SimpleVector		sv		= new SimpleVector(), chooserPos = new SimpleVector();
	public Object3D			terrain, test;
	public Context			con;
	public Texture			text, pos = new Texture(8, 8, new RGBColor(000, 000, 255));
	public long				cnt, time, fps;
	public Camera			cam;
	public int				textsize	= 12, width, height, camX = 15, camY = 5, camH = 35;
	public boolean			loading		= true, alleDa = false, pause = false, debug = true;
	public MapLoader		map;
	public List<Monster>	monster		= Collections.synchronizedList(new LinkedList<Monster>());			;
	public Queue<Tower>		tower		= new ConcurrentLinkedQueue<Tower>();
	public Object3D			monsterOBJ, chooser;
	public Random			rnd			= new Random();
	public SimpleVector		yy, xx, tmp;
	public Thread			worker;
	public Runnable			r;
	public Object3D			animBox;
	public boolean			lock, towerChooser = false;
	Settings setting;

	// Context muss übergeben werden, damit in dieser Klasse die Resourcen
	// geladen werden können
	public Renderer(Context c) {

		con = c;

		
		setting = ((Settings)con.getApplicationContext());
		setting.running = true;
		

		Config.useVBO = false;
		Config.vertexBufferSize = 60000;
		// Config.aggressiveStripping = true;
		// Config.cacheCompressedTextures = true;
		// Config.glDither = false;
		// Config.glIgnoreNearPlane = true;
		// Config.glTrilinear = false;
		// Config.internalMipmapCreation = false;
		Config.skipDefaultShaders = true;
		// Config.stateOrientedSorting = true;
		// Config.reuseTextureBuffers = true;
		// Texture.defaultToMipmapping(false);
		Texture.defaultTo4bpp(true);
		// Config.maxPolysVisible=500;
		// Config.farPlane=60;

	}


	// Wird ausgeführt, sobald der Renderer instantiiert wird, (wohl auch wen
	// der Screen gedreht wird)
	public void onSurfaceChanged( GL10 gl , int w , int h ) {

		// Framebuffer
		//fb = new FrameBuffer(gl, w, h); // GLES 1
		fb = new FrameBuffer( w, h); //GLES 2

		width = w;
		height = h;

		// Selbsterklärend, hier werden alle Objekte erstellt
		world = new ThreadSafeWorld();

		// Umgebungslicht R,G,B; 255,255,255 Maximale Helligkeit
		world.setAmbientLight(150, 150, 150);

		// Ein zusätzliches Licht
		sun = new Light(world);
		sun.enable();

		// Vector für das Licht (sun)
		sv.set(new SimpleVector(0, 0, 8f));
		sun.setPosition(sv);

		// Kamera instantiieren und nach unten blicken lassen
		cam = world.getCamera();
		cam.setPosition(camX, camY, camH);
		cam.rotateCameraX((float) Math.PI / 1.2f);

		r = new Runnable() {

			public void run() {
				try {

					if (!pause && !lock) {
						if (System.currentTimeMillis() - time >= 1000) {
							fps = cnt;
							cnt = 0;
							time = System.currentTimeMillis();
							if (monster.size() < 2 && !alleDa) {
								monster.add(new Monster(map.spawnpunkt.getNext(), monsterOBJ));

							}
						}

						while (lock) {
						}

						for (Tower e : tower) {

							e.update();

						}
						for (int i = 0; i < monster.size(); i++) {

							monster.get(i).update();

						}
					}
					// Thread.yield();
					Thread.sleep(7);

				}
				catch (Exception e) {

					e.printStackTrace();
				}
			}
		};

		worker = new Thread(r, "Tower and Monster") {
			@Override
			public void run() {

				while (setting.running) {
					try {
						r.run();
					}
					catch (Exception e) {

						e.printStackTrace();
						break;
					}
				}
			}
		};

		worker.setDaemon(true);
		worker.setPriority(Thread.MAX_PRIORITY);

		MemoryHelper.compact();
		loading();
	}


	public void onSurfaceCreated( GL10 gl , EGLConfig config ) {
	}


	public SimpleVector convertScreenPixelsTo3D( int x , int y ) {
		SimpleVector direction = new SimpleVector(Interact2D.reproject2D3DWS(world.getCamera(), fb, x, y)).normalize();
		Object[] res = null;

		try {
			res = world.calcMinDistanceAndObject3D(world.getCamera().getPosition(), direction, 10000);
		}
		catch (Exception e) {
			return direction;
		}

		float distance = 0;

		if (res != null) {
			distance = (Float) res[0];

			direction.scalarMul(distance);

			direction.add(world.getCamera().getPosition());
		}

		// Log.e("TEST-JPCT","Point in 3D.. 1: "+direction.toString());
		return direction;
	}


	public void onDrawFrame( GL10 gl ) {

		cnt++;

		// fb.clear(back);

		world.render(fb);

		if (debug) {
			print("FPS: " + fps, textsize, 0, textsize);
			print("FB Resolution: " + width + "x" + height, textsize, 0, 3 * textsize);
			print("Tex Memory: " + (TextureManager.getInstance().getMemoryUsage() / 1024) + "KB", textsize, 0, 4 * textsize);
			print("Monstercount " + monster.size(), textsize, 0, 5 * textsize);
			print("Native Heap " + android.os.Debug.getNativeHeapAllocatedSize() / 1024 + "KB", textsize, 0, 6 * textsize);
			print("Tower Count " + tower.size(), textsize, 0, 7 * textsize);
			print("Mapsize WxH " + map.bitmapWidth + "x" + map.bitmapHeight, textsize, 0, 8 * textsize);
		}

		camX = (int) cam.getPosition().x;
		camY = (int) cam.getPosition().y;

		fb.blit(map.mapTex, 0, map.bitmapHeight, (fb.getWidth() - (fb.getWidth() / 8)), 0, map.bitmapWidth, -map.bitmapHeight, (fb.getWidth() / 8), (fb.getWidth() / 8), 50, false);
		fb.blit(pos, 0, 0, (int) ((fb.getWidth() - (fb.getWidth() / 8)) + (camX * (fb.getWidth() / (8 * map.bitmapWidth)))), (int) ((fb.getWidth() / 8) - camY * (fb.getWidth() / (6 * map.bitmapHeight))), 1, 1, 8, 8, 50, false);
		fb.display();

	}


	// Kamera um Delta x,y,z bewegen

	public void setCameraDiff( float x , float y , float z ) {

		if (!(x == cam.getPosition().x && y == cam.getPosition().y && z == cam.getPosition().z)) {
			cam.setPosition(cam.getPosition().x + x, cam.getPosition().y + y, cam.getPosition().z + z);

		}
	}


	// Hier fehlen noch Sonderzeichen und so weiter
	public void print( String text , int size , int x , int y ) {

		text = text.toUpperCase(java.util.Locale.GERMANY);
		int tmp;

		for (int i = 0; i < text.length(); i++) {

			tmp = (text.charAt(i) - 'A');

			if (text.charAt(i) == '-') {
				fb.blit(this.text, 0, 750, x + (size * i), y, 16, 20, size, size, 255, true);

			}
			else if (tmp <= 26 && tmp >= 0) {
				// fb.blit(null, srcX, srcY, destX, destY, sourceWidth,
				// sourceHeight, destWidth, destHeight, transValue, true);
				fb.blit(this.text, 0, tmp * 21, x + (size * i), y, 16, 20, size, size, 255, true);

			}
			else if (tmp >= -17 && tmp <= -8) {
				fb.blit(this.text, 0, ((text.charAt(i) - '0') * 21) + 543, x + (size * i), y, 16, 20, size, size, 255, true);

			}

		}

	}


	public void loading() {

		Texture loadingTex = new Texture(con.getResources().openRawResource(R.raw.loading));

		Util.loadTexture("loading", loadingTex);

		fb.clear(back);

		fb.blit(loadingTex, 0, 0, 0, 0, loadingTex.getWidth(), loadingTex.getHeight(), width, height, -1, false);
		fb.display();

		// Texturen laden, text muss Klassenvariable sein, da print() diese
		// benötigt
		text = new Texture(BitmapHelper.rescale(BitmapHelper.convert(con.getResources().getDrawable(R.raw.text)), 16, 1024));


		
		
		Util.loadTexture("text", text);
		

		// openRawResource ist Speicher sparender als der andere Kram
		Texture terrainTex = new Texture(con.getResources().openRawResource(R.raw.grasstex));
		Util.loadTexture("terrain", terrainTex);

		Texture hpTex = new Texture(con.getResources().openRawResource(R.raw.hp));

		Util.loadTexture("hpBar", hpTex);
		Util.loadTexture("marker", new Texture(1, 1, new RGBColor(000, 255, 0, 128)));

		
		
			
		System.err.println("Loading Map: " + setting.mapfolder + setting.map);
		
		try {
			File f = new File(setting.mapfolder + setting.map);
			map = new MapLoader(f);
		}
		catch (Exception e) {
			System.err.println("Cannot load Map!");
			e.printStackTrace();

		}

		terrain = new Object3D(2); // Object3D(int maxTriangles)
		terrain.addTriangle(SimpleVector.create(map.bitmapWidth, map.bitmapHeight, 0f), 0f, 0f, SimpleVector.create(0, map.bitmapHeight, 0f), 8f, 0f, SimpleVector.create(map.bitmapWidth, 0f, 0f), 0f, 8f);
		terrain.addTriangle(SimpleVector.create(0f, 0f, 0f), 0f, 0f, SimpleVector.create(map.bitmapWidth, 0f, 0f), 0f, 8f, SimpleVector.create(0f, map.bitmapHeight, 0f), 8f, 0f);
		terrain.setTexture("terrain");
		terrain.setName("Terrain");
		terrain.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);

		Util.loadTexture("btnaddtower", new Texture(BitmapHelper.rescale(BitmapHelper.convert(con.getResources().getDrawable(R.raw.btnaddtower)), 32, 32)));

		// Speicher Optimierung, Texturen sehen trotzdem noch gut aus
		terrainTex.compress();
		text.compress();


		
		
		monsterOBJ = ObjectLoader.loadTowerOBJ(R.raw.boxx, R.raw.boxxmtl, 0.01f, false, R.raw.htex)[0];
				
		chooser = Primitives.getCube(0.5f);
		chooser.setTexture("btnaddtower");
		chooser.setName("Chooser");
		chooser.calcTextureWrap();
		chooser.rotateX((float) (Math.PI / 2));
		chooser.rotateZ((float) (Math.PI / 4));

		map.update();

		world.addObject(terrain);
		world.addObject(chooser);
		world.compileAllObjects();
		world.buildAllObjects();

		loadingTex = null;

		TextureManager.getInstance().removeAndUnload("loading", fb);

		loading = false;
		worker.start();
	}



}
