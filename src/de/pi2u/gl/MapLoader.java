/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pi2u.gl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import de.pi2u.menu.R;
import android.graphics.Bitmap;
import com.threed.jpct.Object3D;
import com.threed.jpct.Texture;
import com.threed.jpct.util.BitmapHelper;

/**
 * 
 * @author root
 */
public class MapLoader {

	public static int			treeCount	= 0;
	public static boolean[][]	grid;										// Matrix
																			// für
																			// die
																			// Wegfindung
	public static int					bitmapHeight = 0, bitmapWidth = 0;
	public Texture				mapTex;

	public Point				end			= new Point(0, 0);
	public RingList<Spawnpoint>	spawnpunkt	= new RingList<Spawnpoint>();
	public Bitmap				bitmap;
	public Object3D[]			tower, tower2;
	public Object3D				bullet;


	public MapLoader(File f) throws Exception {
		// super(c);

		load(f);
	}


	public void load( File f ) throws Exception { 
		try {

			
			bitmap = BitmapHelper.loadImage(new FileInputStream(f));
			System.out.println("Using external map!");

		}
		catch (FileNotFoundException e) {

			try {
				bitmap = BitmapHelper.loadImage(MainActivity.renderer.con.getResources().openRawResource(R.raw.map));
				System.err.println("Cannot load Map, using default one!");
			}
			catch (Exception ee) {
				return;
			}
		}
		catch (Exception e) {
			return;
		}

		bitmapHeight = bitmap.getHeight();
		bitmapWidth = bitmap.getWidth();

		System.out.println("bitmapWidth: " + bitmapWidth + " bitmapHeight: " + bitmapHeight);
		
		grid = new boolean[bitmapWidth][bitmapHeight];

		bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);

		mapTex = new Texture(bitmap);
		mapTex.removeAlpha();
		Util.loadTexture("minimap", mapTex);

		Object3D buffer[] = new Object3D[bitmapHeight * bitmapWidth];
		buffer[0] = com.threed.jpct.Loader.loadSerializedObject(MainActivity.renderer.con.getResources().openRawResource(R.raw.sertree));
		// buffer[0] =
		// com.threed.jpct.Loader.load3DS(con.getResources().openRawResource(R.raw.tree),1)[0];
		buffer[0].translate(0, 0, -3.5f);
		buffer[0].scale(0.2f);
		buffer[0].build();
		buffer[0].strip();
		Texture treeTex = new Texture(MainActivity.renderer.con.getResources().openRawResource(R.raw.treetex));
		treeTex.compress();
		Util.loadTexture("tree", treeTex);
		buffer[0].setTexture("tree");


		tower = ObjectLoader.loadTower3DS( R.raw.tower2, 0.5f, false, R.raw.auv2, R.raw.auv1);
		tower2 = ObjectLoader.loadTower3DS( R.raw.tower1, 0.5f, false, R.raw.tetrahed);
		bullet = com.threed.jpct.Loader.load3DS(MainActivity.renderer.con.getResources().openRawResource(R.raw.bullet), 1)[0];

		Texture bulletTex = new Texture(MainActivity.renderer.con.getResources().openRawResource(R.raw.bark5));
		bulletTex.compress();
		Util.loadTexture("bullet", bulletTex);
		bullet.setTexture("bullet");

		for (int h = 0; h < bitmapHeight; h++) {
			for (int w = 0; w < bitmapWidth; w++) {
				if (bitmap.getPixel(w, h) == android.graphics.Color.BLACK) {

					buffer[++treeCount] = buffer[0].cloneObject();
					// buffer[treeCount].shareCompiledData(buffer[0]);

					buffer[treeCount].translate(w, bitmapHeight - h - 1, 0f);
					MainActivity.renderer.world.addObject(buffer[treeCount]);
					grid[w][bitmapHeight - h - 1] = true;

				}
				else if (bitmap.getPixel(w, h) == android.graphics.Color.RED) {

					new Tower(w, bitmapHeight - h - 1, 0.8f, tower, bullet);

				}
				else if (bitmap.getPixel(w, h) == android.graphics.Color.GREEN) {

					end.x = w;
					end.y = bitmapHeight - h - 1;

				}
				else if (bitmap.getPixel(w, h) == android.graphics.Color.BLUE) {

					spawnpunkt.add(new Spawnpoint(new Point(w, bitmapHeight - h - 1)));

				}
				else if (bitmap.getPixel(w, h) == android.graphics.Color.YELLOW) {

					new Tower(w, bitmapHeight - h - 1, 1, tower2, bullet);

				}

			}
		}

		System.out.println("Spawnpoints: ");

		for (Spawnpoint e : spawnpunkt) {
			e.setEnd(end);
			e.print();
		}

		System.out.println("Endpoint: " + end.toString());

		buffer = null;

	}


	public void update() {

		for (int x = 0; x < bitmapWidth; x++) {
			for (int y = 0; y < bitmapHeight; y++) {

				if (grid[x][y]) {
					bitmap.setPixel(x, y, 0x8fff0000);
				}
				else {
					bitmap.setPixel(x, y, 0x8f00ff00);
				}
			}
		}

		mapTex = null;

		mapTex = new Texture(bitmap);
		mapTex.removeAlpha();

	}
}
