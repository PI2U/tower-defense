package de.pi2u.gl;

import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Object3D;
import com.threed.jpct.World;

public class ThreadSafeWorld extends World {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6134135190408110340L;
	public boolean lock = false;

	public ThreadSafeWorld() {
		super();
	}

	public void remove(Object3D in) {
		while (lock) {}
			lock = true;
			removeObject(in);
			lock = false;
		

	}

	public void render(FrameBuffer fb) {
		while (lock) {}
			lock = true;
			renderScene(fb);
			draw(fb);
			lock = false;
		
	}

}
