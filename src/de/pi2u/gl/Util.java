package de.pi2u.gl;

import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;

public class Util {

	public Util() {
		// TODO Auto-generated constructor stub
	}

	public static void loadTexture(String name , Texture tex){
		
		if (!TextureManager.getInstance().containsTexture(name)) {
			TextureManager.getInstance().addTexture(name, tex);
		}
	}
	
 
	public static SimpleVector tmp = new SimpleVector(0, 0, 0);
	public static SimpleVector xx, yy, center;
	
	public static void lookAt(Object3D src , Object3D target){

		
		double alpha = 0;
		
		center = src.getTransformedCenter();
		yy = src.getXAxis().normalize();
		xx = target.getTransformedCenter().calcSub(center).normalize();
		tmp.x = yy.y * -1;
		tmp.y = yy.x;

		alpha = Math.atan2(xx.x, xx.y) - Math.atan2(tmp.x, tmp.y);

		src.rotateZ((float) alpha);
	}
	
	public static void lookAt(Object3D src ,int x, int y){
		
		double alpha = 0;
		
		center = src.getTransformedCenter();
		yy = src.getXAxis().normalize();
		tmp.set(x, y, 0);
		xx = tmp.calcSub(center).normalize();
		tmp.x = yy.y * -1;
		tmp.y = yy.x;

		alpha = Math.atan2(xx.x, xx.y) - Math.atan2(tmp.x, tmp.y);

		src.rotateZ((float) alpha);
	}	
	
}
