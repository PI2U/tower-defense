/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pi2u.gl;

/**
 * 
 * @author Patrick
 */
public class Point implements Comparable<Point> {
	public int x, y, g, h, f;
	public Point zeigtAuf;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
		// System.out.println("g: " + g + " h: " + h + " x: " +x + " y: " + y);
	}

	public Point(int x, int y, Point vorgaenger) {
		this.x = x;
		this.y = y;
		this.zeigtAuf = vorgaenger;
		// System.out.println("g: " + g + " h: " + h + " x: " +x + " y: " + y);
	}

	public Point(int x, int y, int g, int h, Point vorgaenger) {
		this.x = x;
		this.y = y;
		this.g = g;
		this.h = h;
		this.zeigtAuf = vorgaenger;
		// System.out.println("g: " + g + " h: " + h + " x: " +x + " y: " + y);
	}

	public Point(int x, int y, int g, int h) {

		this(x, y);
		this.g = g;
		this.h = h;
	}

	public void calcF(Point ziel) {
		h = Math.abs( (x - ziel.x)) + Math.abs((y - ziel.y) );
		if (this.diagonal(zeigtAuf)) {
			g = 10 + zeigtAuf.g;
		} else if (x == zeigtAuf.x || y == zeigtAuf.y) {
			g = zeigtAuf.g + 10;
		}

		f = h + g;
	}

	public int calcH(Point o) {

		return  Math.abs( (x - o.x) )  + Math.abs((y - o.y) );
	}

	public boolean diagonal(Point b) {

		if (b.x > x) {
			if (b.y > y || b.y < y) {
				return true;
			}
		} else if (b.x < x) {
			if (b.y > y || b.y < y) {
				return true;
			}
		}

		return false;
	}

	public Point copyTo() {

		return new Point(this.x, this.y, this.g, this.h);
	}

	public boolean equals(Point b) {

		if (x == b.x && y == b.y) {
			return true;
		}
		return false;
	}

	public void zeigeAuf(Point b) {
		zeigtAuf = b;
	}

	@Override
	public int compareTo(Point o) {
		final int BEFORE = -1;
		final int EQUAL = 0;
		final int AFTER = 1;

		if (x == o.x && y == o.y) {
			return EQUAL;
		}
		if (f < o.f) {
			return BEFORE;
		}
		if (f > o.f) {
			return AFTER;
		}
		return 1;

	}

	@Override
	public String toString(){
		return "(" + x +","+y+")";
		
	}
}
