Thank you for downloading this beta version of jBrush for JPCT.

I wrote this software for my own purpose and to aid the JPCT community in their projects. This software is free and can be freely distributed provided that this disclaimer is always attached to it.

This program comes AS IS, without any guarantees or warranty. It has been offered in good faith and any consequential damage due to its use is the sole responsibility of the user operating the software on their machine.

Although I have attempted to find and correct any bugs in this software, I am not responsible for any damage or losses of any kind caused by the use or misuse of the programs.


-------------------------------------------------
Some useful video tutorials on the use of the software can be found here:

http://www.youtube.com/watch?v=yCjsPvN0D3E
http://www.youtube.com/watch?v=wb-BJPOz9UE
http://www.youtube.com/watch?v=sPtNPfJMLRU

Comments and bug reports are appreciated! Feel free to post them on the relevant JPCT forum post:

http://www.jpct.net/forum2/index.php/topic,2346.0.html

Or to email me in case I have not visited the forum for a while.

Alex Marmo (contact: s_marmo@hotmail.com)