Import scene in your JPCT / AE project:

- Find the jbrush_lib and jbrush_ae_lib jar files in jBrush/lib folder.

- Link the one you need (according to JPCT or AE) to your project (properties/build 
path in Eclipse).

- IMPORTANT: Place all your models, textures and the Scene text file in the "assets" 
folder of your project

- Import java.uilt.Vector and com.jbrush* at the top of the class 

- Create the field Vector<EditorObject> objects in the initialization part. For each 
type of additional component you wish to import, initialise the Vector collection: 
"Vector<LightData> lights", "Vector<Trigger> triggers", "Vector<ImageData> 
images".

- objects = Scene.LoadLevel("scene.txt", objects, world) imports the scene (objects) 
into the world. That's it!

- For adding any additional components (lights, triggers, GUIs), precede the 
LoadLevel line with initializations of the Vector collections (lights = new 
Vector<LightData>(), triggers = new Vector<Trigger>() etc...) and then use the 
appropriate version of the LoadLevel method.

- Variations of the LoadLevel method allow you to import lights, triggers, GUIs. 
These are the different versions of loadLevel. You can also import serialized objects
or load for JPCT-AE (Android). For each LoadLevel version there is a corresponding 
LoadSerializedLevel or LoadLevelAE (the latter to use with the jbrush_ae_lib 
library).

- Scene.findObject("myObject", objects) returns a reference to the Object3D for you 
to manipulate it in the game.

- For playing animation created in the editor, place the line 
Animator.EnableAnimations() in the Update loop so that the library will take care of 
the keyframing routines for you.

- Animator.Play(anObject, "walk", objects) plays the animation continuously. 
PlayOnce plays the animation once only, Stop(anObject, objects) stops the 
animation.

- Scene.checkTriggerCollision allows to check if a game entity with given position 
and radius has entered any of the created triggers. A string value is returned 
indicating "null" or the name of the trigger the object collided with.

- Scene.showTriggers displays all the triggers as grey boxes.

- GUI.SetImagePosition or SwitchImage or SetImageSize allow to activate and 
modify the position/dimension of a GUI Image with given name. For the GUIs to be displayed 
place GUI.displayImages in the update loop with a reference to the frame buffer.

Check out the video tutorials for more information and guidance:

http://www.youtube.com/user/blazingangel187